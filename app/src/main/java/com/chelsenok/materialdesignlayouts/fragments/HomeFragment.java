package com.chelsenok.materialdesignlayouts.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.chelsenok.materialdesignlayouts.R;

public class HomeFragment extends MaterialFragment {

    private static SharedPreferences sharedPreferences;
    private static boolean isFirst;

    private static final String DEVELOPER_MODE = "developer_mode";
    private static final String MAGNIFIER = "magnifier";
    private static final String ON_CLICK = "on_click";
    private static final String IS_FIRST = "is_first";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = onCreateView(
                inflater,
                container,
                savedInstanceState,
                R.layout.item_home,
                R.id.toolbarHome
        );

        sharedPreferences = getActivity().getSharedPreferences(getClass().getSimpleName(), Context.MODE_PRIVATE);

        isFirst = sharedPreferences.getBoolean(IS_FIRST, true);
        initializeSwitch(rootView, R.id.switchDeveloperMode, DEVELOPER_MODE);
        initializeSwitch(rootView, R.id.switchMagnifier, MAGNIFIER);
        initializeSwitch(rootView, R.id.switchOnClick, ON_CLICK);
        sharedPreferences.edit().putBoolean(IS_FIRST, false).apply();

        return rootView;
    }

    private void initializeSwitch(final View view, int id, final String memoryName) {
        Switch aSwitch = (Switch) view.findViewById(id);
        aSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            sharedPreferences.edit().putBoolean(memoryName, b).apply();

            if (memoryName.equals(DEVELOPER_MODE)) {
                isDeveloperModeChecked = b;
            } else if (memoryName.equals(MAGNIFIER)) {
                isMagnifierChecked = b;
            } else if (memoryName.equals(ON_CLICK)) {
                isOnClickChecked = b;
            }
        });

        if (memoryName.equals(MAGNIFIER)) {
            aSwitch.setChecked(sharedPreferences.getBoolean(memoryName, false));
        } else {
            aSwitch.setChecked(sharedPreferences.getBoolean(memoryName, isFirst));
        }
    }
}
