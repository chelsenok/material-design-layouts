package com.chelsenok.materialdesignlayouts.fragments.communicate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chelsenok.materialdesignlayouts.R;
import com.chelsenok.materialdesignlayouts.fragments.MaterialFragment;

public class RateMeFragment extends MaterialFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return onCreateView(
                inflater,
                container,
                savedInstanceState,
                R.layout.item_my_files,
                R.id.toolbarMyFiles
        );
    }
}
