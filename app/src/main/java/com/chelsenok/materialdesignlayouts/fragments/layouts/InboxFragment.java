package com.chelsenok.materialdesignlayouts.fragments.layouts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chelsenok.materialdesignlayouts.R;
import com.chelsenok.materialdesignlayouts.fragments.MaterialFragment;

public class InboxFragment extends MaterialFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = onCreateView(
                inflater,
                container,
                savedInstanceState,
                R.layout.item_inbox,
                R.id.toolbarInbox
        );

        setLayoutVisibility(view.findViewById(R.id.layout_frame));
        setLayoutVisibility(view.findViewById(R.id.layout_scroll));
        setLayoutVisibility(view.findViewById(R.id.layout_floating_action_button));

        return view;
    }
}
