package com.chelsenok.materialdesignlayouts.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chelsenok.materialdesignlayouts.R;
import com.chelsenok.materialdesignlayouts.fragments.HomeFragment;

public abstract class MaterialFragment extends Fragment {

    protected static boolean isDeveloperModeChecked;
    protected static boolean isMagnifierChecked;
    protected static boolean isOnClickChecked;

    public static boolean getDeveloperModeChecked() {
        return isDeveloperModeChecked;
    }

    public static boolean getMagnifierChecked() {
        return isMagnifierChecked;
    }

    public static boolean getOnClickChecked() {
        return isOnClickChecked;
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState,
                             int layoutId, int toolbarId) {

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        View rootView = inflater.inflate(layoutId, container, false);

        Toolbar toolbar = (Toolbar) rootView.findViewById(toolbarId);

        activity.setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        return rootView;
    }

    protected void setLayoutVisibility(View layout) {
        if (isDeveloperModeChecked) {
            layout.setVisibility(View.VISIBLE);
        } else {
            layout.setVisibility(View.GONE);
        }
    }
}
