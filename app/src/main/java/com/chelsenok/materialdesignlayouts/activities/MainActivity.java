package com.chelsenok.materialdesignlayouts.activities;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.chelsenok.materialdesignlayouts.R;
import com.chelsenok.materialdesignlayouts.fragments.HomeFragment;
import com.chelsenok.materialdesignlayouts.fragments.MaterialFragment;
import com.chelsenok.materialdesignlayouts.fragments.layouts.InboxFragment;
import com.chelsenok.materialdesignlayouts.fragments.layouts.MyFilesFragment;
import com.chelsenok.materialdesignlayouts.ui.ActivityTransition;
import com.chelsenok.materialdesignlayouts.ui.ActivityViewManager;
import com.chelsenok.materialdesignlayouts.utils.ViewCopier;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static Menu sMenu;

    private static View sCopyView;
    private static Fragment sCurrentFragment;
    private static View sViewProgressBar;
    private static Thread mCopyThread;
    private View mCurrentView;
    private Context mContext;

    private static final HomeFragment HOME_FRAGMENT = new HomeFragment();
    private static final InboxFragment INBOX_FRAGMENT = new InboxFragment();
    private static final MyFilesFragment MY_FILES_FRAGMENT = new MyFilesFragment();

    private static HashMap<Integer, Fragment> sIntegerFragmentHashMap;

    static {
        sIntegerFragmentHashMap = new HashMap<>();
        sIntegerFragmentHashMap.put(R.id.nav_home, HOME_FRAGMENT);
        sIntegerFragmentHashMap.put(R.id.nav_list_default, INBOX_FRAGMENT);
        sIntegerFragmentHashMap.put(R.id.nav_list_expanded, MY_FILES_FRAGMENT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requestWindowFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            requestWindowFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_main);
        sViewProgressBar = findViewById(R.id.progress_bar);

        ActivityViewManager.setTaskDescription(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        sMenu = navigationView.getMenu();

        if (sCurrentFragment == null) {
            setCurrentFragment(HOME_FRAGMENT);
        }
        setFragmentContainer();
        setCurrentMenuItemChecked(true);
    }

    public static View getCurrentViewCopy() {
        return sCopyView;
    }

    private Integer getMenuItemIdFromMap(Fragment fragment) {
        for (Map.Entry<Integer, Fragment> entry : sIntegerFragmentHashMap.entrySet()) {
            if (fragment.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return 0;
    }

    @Nullable
    private Fragment getFragmentFromMap(Integer menuItemId) {
        for (Map.Entry<Integer, Fragment> entry : sIntegerFragmentHashMap.entrySet()) {
            if (menuItemId.equals(entry.getKey())) {
                return entry.getValue();
            }
        }
        return null;
    }

    private void setFragmentContainer() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, sCurrentFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        new Thread(() -> {
            Fragment newFragment = getFragmentFromMap(item.getItemId());
            if (sCurrentFragment != newFragment) {
                setProgressBarVisibilityOnUiThread(View.VISIBLE);
            }
            try {
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (sCurrentFragment != newFragment) {
                setCurrentFragment(newFragment);
                setFragmentContainer();
            }
            setProgressBarVisibilityOnUiThread(View.INVISIBLE);
        }).start();

        return true;
    }

    private void setCurrentFragment(Fragment fragment) {
        setCurrentMenuItemChecked(false);
        sCurrentFragment = fragment;
    }

    private void setProgressBarVisibilityOnUiThread(final int visibility) {
        runOnUiThread(() -> sViewProgressBar.setVisibility(visibility));
    }

    public void setCurrentMenuItemChecked(boolean menuItemChecked) {
        if (sCurrentFragment != null) {
            sMenu.findItem(getMenuItemIdFromMap(sCurrentFragment)).setChecked(menuItemChecked);
        }
    }

    public void onClick(View view) {
        if (MaterialFragment.getOnClickChecked()) {
            mCurrentView = view;
            mContext = this;
            mCopyThread = new Thread(() -> sCopyView = new ViewCopier(mContext, mCurrentView).get());
            mCopyThread.start();
            String transitionName = view.toString();
            view.setTransitionName(transitionName);
            try {
                mCopyThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ActivityTransition.startActivityWithTransition(
                    this, DescriptionActivity.class, view, transitionName
            );
        }
    }
}