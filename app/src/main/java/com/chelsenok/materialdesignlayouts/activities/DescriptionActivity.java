package com.chelsenok.materialdesignlayouts.activities;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.chelsenok.materialdesignlayouts.R;
import com.chelsenok.materialdesignlayouts.ui.ActivityTransition;
import com.chelsenok.materialdesignlayouts.ui.ActivityViewManager;
import com.chelsenok.materialdesignlayouts.utils.ViewXmlParser;

public class DescriptionActivity extends AppCompatActivity {

    private View mView;
    private ScrollView mTextScrollView;
    private LinearLayout mMainLayout;
    private FrameLayout mProgressFrame;
    private Context mContext;
    private final Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            mTextScrollView.addView((View) msg.obj);
            mMainLayout.removeView(mProgressFrame);
            mMainLayout.addView(mTextScrollView);
            return true;
        }
    }
    );
    private static String sXML;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requestWindowFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            requestWindowFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        }

        mContext = this;
        LayoutInflater inflater = LayoutInflater.from(this);
        mMainLayout = (LinearLayout) inflater.inflate(R.layout.activity_description, null);
        setContentView(mMainLayout);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        ActivityViewManager.setStatusBarColor(this, R.color.colorDescriptionBack);
        ActivityViewManager.setTaskDescription(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarDescription);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mView = MainActivity.getCurrentViewCopy();
        int viewWidth = mView.getWidth();
        int displayWidth = getWindowManager().getDefaultDisplay().getWidth();
        int displayHeight = getWindowManager().getDefaultDisplay().getHeight();
        int width = viewWidth;
        if ((viewWidth == displayWidth || viewWidth == displayHeight) && displayHeight != displayWidth) {
            width = ViewGroup.LayoutParams.MATCH_PARENT;
        }
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(width, mView.getHeight(), Gravity.CENTER_HORIZONTAL);
        mView.setLayoutParams(params);
        mView.setClickable(false);

        try {
            ((ViewGroup) mView.getParent()).removeView(mView);
        } catch (Exception ignored) {
        }
        FrameLayout viewFrame = returnFrameLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        viewFrame.addView(mView);
        mProgressFrame = returnFrameLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        mProgressFrame.addView(progressBar);

        mMainLayout.addView(viewFrame);
        mMainLayout.addView(mProgressFrame);
        new Thread(this::createTextScrollView).start();
    }

    private void createTextScrollView() {
        TextView textView = new TextView(mContext);
        if (sXML == null) {
            sXML = ViewXmlParser.getXml(mView);
        }
        textView.setText(sXML);
        textView.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Monospace.ttf");
        textView.setTypeface(type);

        mTextScrollView = new ScrollView(mContext);
        setTextScrollViewParams();
        Message message = new Message();
        message.obj = textView;
        mHandler.sendMessage(message);
    }

    private void setTextScrollViewParams() {
        int dp = 56;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            dp = 48;
        }
        int pixels = convertDpToPixel(this, dp) / 2;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.setMargins(0, pixels, 0, 0);
        mTextScrollView.setLayoutParams(params);
        mTextScrollView.setPadding(pixels, 0, pixels, pixels);
    }

    private int convertDpToPixel(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    private FrameLayout returnFrameLayout(int width, int height) {
        FrameLayout layout = new FrameLayout(this);
        layout.setLayoutParams(new FrameLayout.LayoutParams(width, height));
        return layout;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBack();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        onBack();
    }

    private void onBack() {
        sXML = null;
        ActivityTransition.finishActivityWithTransition(this);
    }
}
