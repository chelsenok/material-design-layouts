package com.chelsenok.materialdesignlayouts.ui;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.chelsenok.materialdesignlayouts.R;

public abstract class ActivityTransition {

    public static void startActivityWithTransition(
            final Activity currentActivity,
            final Class secondActivityClass,
            View view,
            String transitionName
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(currentActivity, view, transitionName);
            currentActivity.startActivity(new Intent(currentActivity, secondActivityClass), options.toBundle());
        } else {
            new Handler().postDelayed(() -> {
                currentActivity.startActivity(new Intent(currentActivity, secondActivityClass));
                currentActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }, getAnimationTime(currentActivity));
        }
    }

    public static void finishActivityWithTransition(final Activity currentActivity) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            currentActivity.finishAfterTransition();
//        } else {
        new Handler().postDelayed(() -> {
            currentActivity.finish();
            currentActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }, getAnimationTime(currentActivity));
//        }
    }

    private static int getAnimationTime(Activity activity) {
        int inDuration = getAnimationDuration(activity, R.anim.fade_in);
        int outDuration = getAnimationDuration(activity, R.anim.fade_out);

        return Math.max(inDuration, outDuration);
    }

    private static int getAnimationDuration(Activity activity, int animId) {
        Animation animation = AnimationUtils.loadAnimation(activity, animId);
        return (int) animation.getDuration();
    }
}
