package com.chelsenok.materialdesignlayouts.utils;

import android.view.View;

import java.util.concurrent.TimeUnit;

public abstract class ViewXmlParser {

    public static View getView(String xml) {
        return null;
    }

    public static String getXml(View view) {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (view != null) {
            return "android:layout_height=\"wrap_content\"";
        }
        return null;
    }
}