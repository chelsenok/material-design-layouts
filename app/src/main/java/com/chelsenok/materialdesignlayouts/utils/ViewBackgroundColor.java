package com.chelsenok.materialdesignlayouts.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.view.View;

public abstract class ViewBackgroundColor {

    public static int get(Context context, View view) {
        try {
            ColorDrawable viewColor = (ColorDrawable) view.getBackground();
            if (viewColor != null) {
                return viewColor.getColor();
            }
            return get(context, (View) view.getParent());

        } catch (Exception e) {
            TypedArray array = context.getTheme().obtainStyledAttributes(new int[]{
                    android.R.attr.colorBackground,
                    android.R.attr.textColorPrimary,
            });
            int backgroundColor = array.getColor(0, 0xFF00FF);
            array.recycle();
            return backgroundColor;
        }
    }
}
