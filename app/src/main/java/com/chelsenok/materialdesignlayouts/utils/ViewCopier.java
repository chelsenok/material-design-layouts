package com.chelsenok.materialdesignlayouts.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class ViewCopier {

    private Class mClass;
    private Context mContext;

    private View mViewReal;
    private View mViewCopy;

    private Method[] mInvokeMethods;
    private Pair<Class, Method[]>[] mViewClassesEtters;

    private class Etter {
        public String name;
        public Method setter;
        public ArrayList<Method> getters;

        public Etter(String name, Method setter) {
            this.name = name;
            this.setter = setter;
            getters = new ArrayList<>();
        }
    }

    private Etter mCurrentEtter;
    private ArrayList<Etter> mEtters;
    private ArrayList<Method> mGetters;
    private final String[] SIDES = new String[]{"Left", "Top", "Right", "Bottom"};

    public ViewCopier(Context context, View view) {
        mContext = context;
        mViewReal = view;
        try {
            mInvokeMethods = new Method[]{
                    ViewCopier.class.getDeclaredMethod("standardInvoke"),
                    ViewCopier.class.getDeclaredMethod("arrayInvoke"),
                    ViewCopier.class.getDeclaredMethod("sideInvoke")
            };
            mViewClassesEtters = new Pair[]{
                    new Pair(TextView.class, new Method[]{
                            ViewCopier.class.getDeclaredMethod("setTextSize"),
                            ViewCopier.class.getDeclaredMethod("setTextColor")
                    }),
                    new Pair(ImageView.class, new Method[]{
                            ViewCopier.class.getDeclaredMethod("setImageDrawable")
                    }),
                    new Pair(View.class, new Method[]{
                            ViewCopier.class.getDeclaredMethod("setBackgroundColor")
                    })
                    // TODO: 04.04.2017 other <T extends View> classes with exclusions
            };
        } catch (Exception ignored) {
        }
    }

    @Nullable
    public View get() {
        Class currentClass = mViewReal.getClass();
        for (Pair pair : mViewClassesEtters) {
            Class aClass = (Class) pair.first;
            if (currentClass.equals(aClass) | isChild(currentClass, aClass)) {
                mClass = aClass;
                break;
            }
        }
        return view();
    }

    private boolean isChild(Class childClass, Class parentClass) {
        Class superClass = childClass.getSuperclass();
        return !superClass.equals(Object.class) && (superClass.equals(parentClass) | isChild(superClass, parentClass));
    }

    private <T extends View> T view() {
        try {
            this.mViewCopy = (T) mClass.getDeclaredConstructor(Context.class).newInstance(mContext);
        } catch (Exception e) {
            return null;
        }

        Method[] classMethods = mClass.getMethods();
        mEtters = new ArrayList<>();
        mGetters = new ArrayList<>();
        for (Method method :
                classMethods) {
            String type = method.getName().substring(0, 3);
            if (type.equals("set")) {
                mEtters.add(new Etter(method.getName().substring(3), method));
            } else if (type.equals("get")) {
                mGetters.add(method);
            }
        }
        for (Etter etter :
                mEtters) {
            for (int i = 0; i < mGetters.size(); i++) {
                Method getter = mGetters.get(i);
                String name = getter.getName().substring(3);
                if (name.equals(etter.name) || isNameSideType(name, etter.name)) {
                    etter.getters.add(getter);
                }
            }
            if (etter.getters.size() != 0) {
                invoke(etter);
            }
        }
        for (Pair<Class, Method[]> pair :
                mViewClassesEtters) {
            Class aClass = mViewCopy.getClass();
            if (aClass.equals(pair.first) || isChild(aClass, pair.first)) {
                for (Method method :
                        pair.second) {
                    try {
                        method.invoke(this);
                    } catch (Exception ignored) {
                    }
                }
            }
        }
        return (T) mViewCopy;
    }

    private boolean isNameSideType(String nameCheck, String nameOrigin) {
        for (String side :
                SIDES) {
            if (nameOrigin.concat(side).equals(nameCheck)) {
                return true;
            }
        }
        return false;
    }

    private void invoke(Etter etter) {
        mCurrentEtter = etter;
        for (Method invokeMethod :
                mInvokeMethods) {
            try {
                if ((boolean) invokeMethod.invoke(this)) {
                    break;
                }
            } catch (Exception ignored) {
            }
        }
    }

    private boolean standardInvoke() throws Exception {
        for (Method method :
                mCurrentEtter.getters) {
            try {
                mCurrentEtter.setter.invoke(mViewCopy, method.invoke(mViewReal));
                mGetters.remove(method);
                return true;
            } catch (Exception ignored) {
            }
        }
        throw new Exception();
    }

    private boolean arrayInvoke() throws Exception {
        for (Method method :
                mCurrentEtter.getters) {
            try {
                Object[] objs = (Object[]) method.invoke(mViewReal);
                mCurrentEtter.setter.invoke(mViewCopy, objs[0], objs[1], objs[2], objs[3]);
                mGetters.remove(method);
                return true;
            } catch (Exception ignored) {
            }
        }
        throw new Exception();
    }

    private boolean sideInvoke() throws Exception {
        Method[] sides = new Method[4];
        for (Method method :
                mCurrentEtter.getters) {
            String name = method.getName();
            for (int i = 0; i < 4; i++) {
                if (name.contains(SIDES[i])) {
                    sides[i] = method;
                }
            }
        }
        mCurrentEtter.setter.invoke(mViewCopy,
                sides[0].invoke(mViewReal),
                sides[1].invoke(mViewReal),
                sides[2].invoke(mViewReal),
                sides[3].invoke(mViewReal)
        );
        for (Method method :
                sides) {
            mGetters.remove(method);
        }
        return true;
    }

    private void setTextSize() {
        ((TextView) mViewCopy).setTextSize(TypedValue.COMPLEX_UNIT_PX, ((TextView) mViewReal).getTextSize());
    }

    private void setTextColor() {
        ((TextView) mViewCopy).setTextColor(((TextView) mViewReal).getCurrentTextColor());
    }

    private void setBackgroundColor() {
        mViewCopy.setBackgroundColor(ViewBackgroundColor.get(mContext, mViewReal));
    }

    private void setImageDrawable() {
        ((ImageView) mViewCopy).setImageDrawable(((ImageView) mViewReal).getDrawable());
    }
}
